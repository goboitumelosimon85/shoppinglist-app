package com.example.shoppinglist.ui.shoppingList

import com.example.shoppinglist.data.db.entities.shoppingItem

interface AddDialogListener {

    fun onAddButtonClicked(item: shoppingItem)
}