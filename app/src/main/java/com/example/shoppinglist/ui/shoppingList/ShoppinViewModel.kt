package com.example.shoppinglist.ui.shoppingList

import androidx.lifecycle.ViewModel
import com.example.shoppinglist.data.db.entities.shoppingItem
import com.example.shoppinglist.data.repositories.ShoppingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class ShoppinViewModel(
    private val repository: ShoppingRepository
): ViewModel() {

    fun upsert(item: shoppingItem) = CoroutineScope(Dispatchers.Main).launch {
        repository.upsert(item)
    }
    fun delete(item: shoppingItem) =  CoroutineScope(Dispatchers.Main).launch {
        repository.delete(item)
    }
    fun getAllShoppingItems() = repository.getAllShoppingItems()
}